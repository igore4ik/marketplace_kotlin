package com.example.marketplace_kotlin


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.marketplace_kotlin.api.ApiMethods
import com.example.marketplace_kotlin.api.ProductJson
import com.example.marketplace_kotlin.databinding.ItemBinding
import com.squareup.picasso.Picasso

class ItemAdapter: RecyclerView.Adapter<ItemAdapter.ItemHolder>() {

        var methodGet = ApiMethods()
    private val productsList: MutableLiveData<List<ProductJson>> = MutableLiveData()
    suspend fun fetchProducts(){
        productsList.value = methodGet.getAllProducts().body()
    }

    private val arrItems: List<ProductJson> = listOf(
       ProductJson( 1,
       "Ігрова приставка PS5 PlayStation 5",
     500,
     "SSD-накопитель: 825 ГБ Скорость чтения: до 5.5 ГБ/с Оптический привод: Ultra HD Blu-ray (66G/100G) ~10xCAV BD-ROM (25G/50G) ~8xCAV BD-R/RE (25G/50G) ~8xCAV DVD ~3.2xCLV Диски: Ultra HD Blu-ray, до 100 ГБ Видеовыход: Порт HDMI",
    "https://myroslavblob.blob.core.windows.net:443/marketplace-public/d8b77bfa-d2b1-4412-9fdd-45fc93548915.jpg"
    ),
       ProductJson(2,
         "Xbox",
         500,
           "Xbox — це гральна консоль шостого покоління та перша в серії консолей Xbox, що випускаються компанією Microsoft. Вона вийшла 15 листопада 2001 року в Північній Америці, а потім в Австралії, Європі та Японії в 2002 році. Це був перший вихід Microsoft на ринок гральних консолей. Консоль конкурувала з Sony PlayStation 2 та Nintendo GameCube. Це була також перша консоль, вироблена американською компанією, з часу Atari Jaguar, яка перестала випускатися в 1996 році.",
         "https://myroslavblob.blob.core.windows.net:443/marketplace-public/ecafbd76-7715-478b-8274-c7f497846c43.jpg"
    ),
       ProductJson(
         3,
         "Iphone 13",
         900,
         "Камера 12 Мп/Діафрагма ƒ/2.2/Режим «Портрет» з поліпшеним ефектом боке та функцією «Глибина»/Портретне освітлення (шість варіантів: Природне світло, Студійне світло, Контурне світло, Сценічне світло, Сценічне світло - ч/б, Світла тональність - ч/б)/Animoji і Memoji/Нічний режим/Технологія Deep Fusion/Smart HDR 4/Фотографічні стилі/Формат Apple ProRAW/Режим «Кіноефект» для знімання відео з малою глибиною різкості (1080p з частотою 30 кадрів у секунду)/Знімання HDR-відео у стандарті Dolby Vision до 4 K з частотою 60 кадрів у секунду/Знімання відео 4 K з частотою 24, 25, 30 або 60 кадрів у секунду/Знімання HD-відео 1080p з частотою 25, 30 або 60 кадрів у секунду/Знімання відео ProRes до 4 K з частотою 30 кадрів у секунду/(1080p з частотою 30 кадрів у секунду для моделі 128 ГБ)*/Запис уповільненого відео 1080р з частотою 120 кадрів у секунду/Режим «Таймлапс» зі стабілізацією зображення/Відео «Таймлапс» у Нічному режимі/Кінематографічна стабілізація відео (4K, 1080p і 720p)/Функція QuickTake/Широкий колірний діапазон для фотографій і Live Photos/Корекція спотворень об'єктива/Спалах Retina Flash/Автоматична стабілізація зображення/Серійна зйомка",
         "https://myroslavblob.blob.core.windows.net:443/marketplace-public/47ff9eca-2959-4653-9497-a1d1e2fe95d3.png"
    ),
ProductJson(
         9,
        "jablko",
        355,
         "jablko",
         null
),
ProductJson(
         67,
         "iphone 4s",
         230,
        "Iphone for Stive",
        null
),
ProductJson(
        68,
        "Iphone 8",
         300,
         "Best phone ever",
         null
),
ProductJson(
         69,
         "Iphone 8",
         300,
         "Best phone ever",
         null
),
ProductJson( 20,
         "MacBook Pro 2021",
         1999,
         "Процессор M1 Pro, коннектор MagSafe, дисплей 120 Гц и… вырез. Новый MacBook Pro 14 получил мощный процессор Apple M1 Pro и mini-LED дисплей с частотой 120 Гц. После таких неудачных экспериментов, как TouchBar и клавиатура butterfly, Apple наконец решила оснастить MacBook действительно полезными для пользователей компонентами. Таким образом у новинки появился картридер, видеовыход HDMI и коннектор MagSafe.",
         "https://myroslavblob.blob.core.windows.net:443/marketplace-public/f321f418-0740-4160-bb76-1d85c1857e51.jpg"
),
ProductJson( 34,
         "Butterfly 3000",
         30,
         "Music Album",
         null
),
ProductJson(30,
        "Gibson Les Paul",
         500,
         "Gibson Les Paul Guitar",
        null
)
    )
    class ItemHolder(item: View): RecyclerView.ViewHolder(item) {

        private val binding = ItemBinding.bind(item)
        fun bind(item: ProductJson) = with(binding){
            var urlPic  = "https://cdn.pixabay.com/photo/2013/07/13/10/45/cracks-157707_960_720.png"
            if(item.picture != null)urlPic = item.picture
            Picasso.get().load(urlPic).into(productImg)
            productDesc.text = item.description
            (item.price.toString()+"$").also { productPrice.text = it }
            productTitle.text = item.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent,false)
        return ItemHolder(view)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(arrItems[position])
    }

    override fun getItemCount(): Int {
        return arrItems.size
    }


}

