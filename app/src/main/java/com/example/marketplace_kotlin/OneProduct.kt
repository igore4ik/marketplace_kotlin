package com.example.marketplace_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.TextView
import org.w3c.dom.Text

class OneProduct : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_product)
        setId()
    }

   private fun setId () {
       val desc = findViewById<TextView>(R.id.item_desc)
       desc.movementMethod =  ScrollingMovementMethod();
        val productId = intent.getIntExtra("id",21)
        val textSetId = findViewById<TextView>(R.id.textView5)
       if (productId >= 0 )
        textSetId.text = productId.toString()
    }

}
