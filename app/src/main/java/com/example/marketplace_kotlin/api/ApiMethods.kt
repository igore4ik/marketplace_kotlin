package com.example.marketplace_kotlin.api

import retrofit2.Response

class ApiMethods {
    suspend fun getAllProducts(): Response<List<ProductJson>> {
        return  RetrofitInstance.api.getProducts()
    }
}