package com.example.marketplace_kotlin.api
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("api/Products?page=1&perPage=15")
    suspend fun getProducts(): Response<List<ProductJson>>
}