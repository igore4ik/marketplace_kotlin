package com.example.marketplace_kotlin.api

data class ProductJson(
    val id: Int,
    val title: String,
    val price: Int,
    val description: String,
    val picture: String?,
)